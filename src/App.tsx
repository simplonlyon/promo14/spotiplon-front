
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import { OneAlbumPage as OneAlbumPage } from './pages/OneAlbumPage';
import { HomePage } from './pages/HomePage';

export interface RouterParams {
  id:string
}

function App() {
  return (
    <BrowserRouter>
    <div>
      <Switch>
        <Route path="/" exact>
          <HomePage />
        </Route>
        <Route path="/album/:id">
          <OneAlbumPage />
        </Route>
      </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
