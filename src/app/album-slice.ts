import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { Album } from "../entities";
import { AppDispatch } from "./store";

interface AlbumState {
    list:Album[];
    oneAlbum:Album|null;
}

const initialState:AlbumState = {
    list:[],
    oneAlbum: null
}

const albumSlice = createSlice({
    name: 'album',
    initialState,
    reducers: {
        setAlbums(state, {payload}) {
            state.list = payload;
        },
        setOneAlbum(state, {payload}) {
            state.oneAlbum = payload;
        }
    }
});

export const {setAlbums, setOneAlbum} = albumSlice.actions;

export default albumSlice.reducer;


export const fetchAlbums = () => async (dispatch:AppDispatch) => {
    try {
        const response = await axios.get('/api/album');
        
        dispatch(setAlbums(response.data));
    } catch (error) {
        console.log(error);
    }
}

export const fetchOneAlbum = (id:number) => async (dispatch:AppDispatch) => {
    try {
        const response = await axios.get('/api/album/'+id);
        
        dispatch(setOneAlbum(response.data));
    } catch (error) {
        console.log(error);
    }
}