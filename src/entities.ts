export interface Album {
    id?:number;
    title?:string;
    released?:string;
    image?:string;
    songs?:Song[];
    artists?:Artist[];
}

export interface Artist {
    id?:number;
    name?:string;
    image?:string;
    songs?:Song[];
    albums?:Album[];
}

export interface Song {
    id?:number;
    title?:string;
    released?:string;
    albums?:Album[];
    artists?:Artist[];
}
