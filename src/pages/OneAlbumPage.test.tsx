import { render } from "@testing-library/react"
import { Provider } from "react-redux";
import { MemoryRouter, Route } from "react-router-dom"
import { store } from "../app/store";
import { OneAlbumPage } from "./OneAlbumPage"




test('It should display a specific album', async () => {

    const { findByText, findAllByText } = render(
        <Provider store={store}>
            <MemoryRouter initialEntries={['album/1']}>
                <Route path='album/:id'>
                    <OneAlbumPage />
                </Route>
            </MemoryRouter>
        </Provider>
    );

    expect(await findByText(/Test Album/)).toBeInTheDocument();
    expect(await findByText(/Test Artist/)).toBeInTheDocument();
    expect(await findAllByText(/Test Song/)).toHaveLength(10);

})