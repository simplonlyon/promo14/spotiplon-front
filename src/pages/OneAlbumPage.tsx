import { useEffect } from "react";
import { Col, Container, Image, Row, Spinner } from "react-bootstrap";
import { useParams } from "react-router-dom"
import { RouterParams } from "../App";
import { fetchOneAlbum } from "../app/album-slice";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { SongList } from "../components/SongList";


export function OneAlbumPage() {
    const { id } = useParams<RouterParams>();
    const album = useAppSelector(state => state.album.oneAlbum);
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(fetchOneAlbum(Number(id)));
    }, [dispatch]);


    if (!album) {
        return <p>Loading...</p>
    }

    return <Container fluid>
        <Row className="justify-content-center">
            <Col xs={10}>
                <Row>
                    <Col sm={4}>
                        <Image fluid src={album.image} alt={album.title} rounded />
                    </Col>
                    <Col sm={8}>
                        <h1>{album.title}</h1>
                        {album.artists?.map(artist => <p key={artist.id}>
                            <Image width={60} src={artist.image} alt={artist.name} roundedCircle />
                            {artist.name}
                        </p>)}
                    </Col>
                </Row>
            </Col>
            {album.songs &&
                <Col xs={10}>
                    <SongList songs={album.songs} />
                </Col>
            }
        </Row>
    </Container>
}