// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom/extend-expect';
import { rest } from 'msw';
import { setupServer } from 'msw/node';

const handlers = [
    rest.get('/api/album', (req, res, ctx) => {

        return res(
            ctx.json([
                {
                    id: 1,
                    title: 'Test Album',
                    image: '/fakeimg.jpg',
                    released: '2021-10-01',
                    artists: [
                        { id: 1, name: 'Test Artist' },
                        { id: 2, name: 'Test Artist 2' },
                    ]
                },
                {
                    id: 2,
                    title: 'Test Album 2',
                    image: '/fakeimg.jpg',
                    released: '2021-10-02',
                    artists: [
                        { id: 1, name: 'Test Artist' },
                        { id: 2, name: 'Test Artist 2' },
                    ]
                }
            ])
        )
    }),
    rest.get('/api/album/1', (req, res, ctx) => {

        return res(
            ctx.json({ "id": 1, "title": "Test Album", "image": "http://placeimg.com/640/480/nature", "released": "2020-11-30", "artists": [{ "id": 4, "name": "Test Artist", "image": "https://cdn.fakercloud.com/avatars/marcomano__128.jpg" }], "songs": [{ "id": 31, "title": "Test Song", "file": "/fakefile.mp3", "released": "2021-03-21" }, { "id": 32, "title": "Test Song", "file": "/fakefile.mp3", "released": "2020-09-10" }, { "id": 33, "title": "Test Song", "file": "/fakefile.mp3", "released": "2021-01-01" }, { "id": 34, "title": "Test Song", "file": "/fakefile.mp3", "released": "2021-07-15" }, { "id": 35, "title": "Test Song", "file": "/fakefile.mp3", "released": "2021-07-19" }, { "id": 36, "title": "Test Song", "file": "/fakefile.mp3", "released": "2020-08-27" }, { "id": 37, "title": "Test Song", "file": "/fakefile.mp3", "released": "2021-06-08" }, { "id": 38, "title": "Test Song", "file": "/fakefile.mp3", "released": "2021-02-04" }, { "id": 39, "title": "Test Song", "file": "/fakefile.mp3", "released": "2021-02-13" }, { "id": 40, "title": "Test Song", "file": "/fakefile.mp3", "released": "2021-06-21" }] })
        );
    })
];


const server = setupServer(...handlers);

beforeAll(() => {
    // Enable the mocking in tests.
    server.listen()
})

afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers()
})

afterAll(() => {
    // Clean up once the tests are done.
    server.close()
})