import { render } from "@testing-library/react"
import { Song } from "../entities"
import { SongList } from "./SongList"

const songs:Song[] = [
    {id: 1, title: 'Song 1', released: '2021-10-04'},
    {id: 2, title: 'Song 2', released: '2021-10-04'},
    {id: 3, title: 'Song 3', released: '2021-10-04'}
];

test('It should display song list', () => {

    const {getAllByText} = render(
        <SongList songs={songs} />
    );

    expect(getAllByText(/Song/)).toHaveLength(3);
})