import { useEffect } from "react";
import { CardGroup, Col, Row } from "react-bootstrap";
import { fetchAlbums } from "../app/album-slice";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { ListedAlbum } from "./ListedAlbum";



export function AlbumList() {
    const albums = useAppSelector(state => state.album.list);
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(fetchAlbums());
    }, [dispatch]);

    return (
        <>
            <Row xs={12} md={4} >
                {albums.map(item => <Col key={item.id}><ListedAlbum album={item} /></Col>)}
            </Row>
        </>
    );
}