import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import { store } from "../app/store";
import { AlbumList } from "./AlbumList";


test('Display all fetched albums', async () => {

    const { findAllByText } = render(
        <Provider store={store}>
            <AlbumList />
        </Provider>
    );

    expect(await findAllByText(/Test Album/)).toHaveLength(2);

});