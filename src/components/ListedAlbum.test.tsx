import { render } from "@testing-library/react"
import { Album } from "../entities";
import { ListedAlbum } from "./ListedAlbum";


const album: Album = {
    id: 1,
    title: 'Test Album',
    image: '/fakeimg.jpg',
    released: '2021-10-01',
    artists: [
        { id: 1, name: 'Test Artist' },
        { id: 2, name: 'Test Artist 2' },
    ]
}

test('Displaying album informations', () => {

    const {getByText, getByAltText} = render(
        <ListedAlbum album={album} />
    );

    expect(getByText('Test Album')).toBeInTheDocument();
    expect(getByAltText('Test Album')).toBeInTheDocument();
    expect(getByText('2021-10-01')).toBeInTheDocument();
    expect(getByText('Test Artist')).toBeInTheDocument();
    expect(getByText('Test Artist 2')).toBeInTheDocument();
})