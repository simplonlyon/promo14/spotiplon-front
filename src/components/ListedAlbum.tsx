import { Card } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import { Album } from "../entities";

interface Props {
    album: Album;
}

export function ListedAlbum({ album }: Props) {

    return (
        <Card>
            <Link to={'/album/' + album.id}>
                <Card.Img variant="top" alt={album.title} src={album.image} />
            </Link>

            <Card.Body>
                <Card.Title>{album.title}</Card.Title>
                <Card.Text>
                    {album.artists?.map(artist =>
                        <span key={artist.id}>{artist.name}</span>
                    )}
                </Card.Text>
            </Card.Body>
            <Card.Footer>
                <small className="text-muted">{album.released}</small>
            </Card.Footer>
        </Card>
    );
}