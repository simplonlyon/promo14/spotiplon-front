import { ListGroup } from "react-bootstrap";
import { Song } from "../entities";


interface Props {
    songs: Song[]
}

export function SongList({ songs }: Props) {


    return (
        <ListGroup>
            {songs.map(song =>
                <ListGroup.Item key={song.id}>{song.title}</ListGroup.Item>
            )}

        </ListGroup>
    )
}